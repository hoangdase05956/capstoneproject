//package com.fpt.controller;
//
//import org.springframework.social.connect.ConnectionRepository;
//import org.springframework.social.facebook.api.Facebook;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//@Controller
//public class SocialController {
//    private Facebook facebook;
//    private ConnectionRepository connectionRepository;
//
//    public SocialController(Facebook facebook, ConnectionRepository connectionRepository) {
//        this.facebook = facebook;
//        this.connectionRepository = connectionRepository;
//    }
//
//    @RequestMapping(value = "feed", method = RequestMethod.GET)
//    public String feed(Model model) {
//
//        if(connectionRepository.findPrimaryConnection(Facebook.class) == null) {
//            return "redirect:/connect/facebook";
//        }
//        
//        
//        System.out.println("====================="+ facebook.userOperations().getUserProfile());
//
////        Users userProfile = facebook.userOperations().getUserProfile();
////        model.addAttribute("userProfile", userProfile);
////        PagedList<Post> userFeed = facebook.feedOperations().getFeed();
////        model.addAttribute("userFeed", userFeed);
//        return "feed";
//    }
//}