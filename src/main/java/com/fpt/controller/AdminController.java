package com.fpt.controller;

import java.sql.Timestamp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fpt.dao.UserDao;
import com.fpt.service.CurrentUser;
import com.fpt.service.TwilioMessageCreator;

@Controller

@RequestMapping("/admin/")
public class AdminController {

	@Autowired
	UserDao userDaoimpl;

	@Autowired
	CurrentUser user;

	@Autowired
	TwilioMessageCreator twilioMessageCreator;
	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	// /admin/Dashboard
	@GetMapping(value = "Dashboard")
	public String SignIn(ModelMap model, HttpSession session) {
		session.setAttribute("user", user.getCurrentUsers());
		return "admin/dashboard";
	}

//		 /admin/list-user
	@GetMapping(value = "list-user")
	public String listUser(ModelMap model) {
		model.addAttribute("users", userDaoimpl.list(1));
		return "admin/user/list_user";
	}

	// admin/list-user-block
	@GetMapping(value = "list-user-block")
	public String listUserBlock(ModelMap model) {
		model.addAttribute("users", userDaoimpl.list(5));
		return "admin/user/list_user_block";
	}

	@GetMapping(value = "list-user-block/{id}")
	public String blockUser(ModelMap model, @PathVariable("id") int id) {
		if (userDaoimpl.userBlock(id, 5)) {

			model.addAttribute("users", userDaoimpl.list(5));
			twilioMessageCreator.sendSMS("Tài khoản của bạn đã bị khoá vào lúc:" + timestamp,
					userDaoimpl.findByID(id).getPhone());

			model.addAttribute("msg", "Khoá thành công.");
			model.addAttribute("class_name", "msg_success");

			return "admin/user/list_user_block";
		} else {
			model.addAttribute("msg", "Khoá thất bại.");
			model.addAttribute("class_name", "msg_error");

			return "admin/user/list_user";
		}
	}

	@GetMapping(value = "list-user/{id}")
	public String UnblockUser(ModelMap model, @PathVariable("id") int id) {
		if (userDaoimpl.userBlock(id, 1)) {
			twilioMessageCreator.sendSMS("Tài khoản của bạn đã được mở khoá vào lúc:" + timestamp,
					userDaoimpl.findByID(id).getPhone());
			model.addAttribute("users", userDaoimpl.list(1));
			model.addAttribute("msg", "Huỷ khoá thành công.");
			model.addAttribute("class_name", "msg_success");

			return "admin/user/list_user";
		} else {
			model.addAttribute("msg", "Huỷ khoá  thất bại.");
			model.addAttribute("class_name", "msg_error");

			return "admin/user/list_user_block";
		}
	}

	// admin/edit-profile
	@GetMapping(value = "edit-profile/{id}")
	public String editProfile(ModelMap model, @PathVariable("id") int id) {

		model.addAttribute("user_detail", userDaoimpl.findByID(id));
		return "admin/user/edit_profile";
	}

	@PostMapping(value = "edit-role/{id}")
	public String editRole(ModelMap model, @PathVariable("id") int id, @RequestParam("role") int role) {
		System.out.println("====================" + id);
		System.out.println("====================" + role);

		if (userDaoimpl.changeRole(id, role)) {

			model.addAttribute("users", userDaoimpl.list(1));
			model.addAttribute("msg", "Uỷ quyền thành công .");
			model.addAttribute("class_name", "msg_success");

			return "admin/user/list_user";

		} else {
			model.addAttribute("msg", "Uỷ quyền thất bại.");
			model.addAttribute("class_name", "msg_error");

			return "admin/user/list_user";
		}

	}

}
