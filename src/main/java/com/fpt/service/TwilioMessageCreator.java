package com.fpt.service;

import org.springframework.stereotype.Service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class TwilioMessageCreator {
	public static final String ACCOUNT_SID = "ACb9713248a0795d608ecf96aae8bb1eb6";
	public static final String AUTH_TOKEN = "155938cf60288c2f705e58dcb6961360";
	public static final String TWILIO_NUMBER = "+12562578741";

	public void sendSMS(String msg, String phone) {

		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

		Message message = Message.creator(new PhoneNumber("+84" + phone.substring(1)), // to
				new PhoneNumber(TWILIO_NUMBER), // from
				msg).create();

		System.out.println(message.getSid());
	}

}
