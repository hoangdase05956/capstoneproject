<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<jsp:include page="layout/_header.jsp"></jsp:include>



<style>
/* .pb-cmnt-container {
	font-family: Lato;
}

.pb-cmnt-textarea {
	resize: none;
	height: 130px;
	width: 100%;
	border: 1px solid #F2F2F2;
}

/* 

</style>

<div id="content" class="clearfix " style="width: 100%;">
	<div id="left-area" class="clearfix" style="width: 91%;">

		<div class="post-53 post category-barbeque" id="post-53">


			<h3 style="font-size: 30px; text-align: center; margin: 30px 0px;"
				class="single-post-title">Best Bread pairing for Barbeque?</h3>


			<div class="post-53 post category-barbeque" style="width: 100%;">
				<div class="blog-div with20">
					<h3 style="text-align: center;">Thời gian</h3>
					<h5 style="text-align: center;">35 Phút</h5>
				</div>
				<div class="blog-div with20">
					<h3 style="text-align: center;">Khẩu phần</h3>
					<h5 style="text-align: center;">4 người</h5>

				</div>

				<div class="blog-div with20">
					<h3 style="text-align: center;">Cách thức</h3>
					<h5 style="text-align: center;">Kho</h5>

				</div>

				<div class="blog-div with20">
					<h3 style="text-align: center;">Mức độ</h3>
					<h5 style="text-align: center;">Trung bình</h5>


				</div>

				<div class="blog-div with20">
					<h3 style="text-align: center;">Giá thành</h3>
					<h4 style="text-align: center;">50k - 70k</h4>

				</div>



			</div>








			<div class="post-thumb single-img-box" style="width: 100%;">
				<a title="Best Bread pairing for Barbeque?"> <iframe width="889"
						height="400"
						src="https://www.youtube.com/embed/PG34mzEe8os?list=RDPG34mzEe8os"
						frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>

				</a>
			</div>


			<div style="width: 100%; height: 60px;">
				<div style="width: 50%; float: left; height: 40px;">
					<img style="width: 40px; margin-left: 270px;"
						src="${pageContext.request.contextPath}/resources/images/heart.png" />
					<span style="font-weight: 600;">100 Lượt thích </span>
				</div>

				<div style="width: 50%; float: left; height: 40px;">
					<img style="width: 40px;"
						src="${pageContext.request.contextPath}/resources/images/eyes.png" />
					<span style="font-weight: 600;">100 Lượt xem </span>
				</div>




			</div>


			<div class="blog-div with30">
				<h3 style="text-align: center;">Xuất xứ</h3>
				<h5 style="text-align: center;">Việt Nam</h5>
			</div>
			<div class="blog-div with30">
				<h3 style="text-align: center;">Ngày lễ/Kỉ</h3>
				<h5 style="text-align: center;">Sinh nhật</h5>

			</div>

			<div class="blog-div with30">
				<h3 style="text-align: center;">Thể loại</h3>
				<h5 style="text-align: center;">Món chính</h5>

			</div>

			<div class="blog-div with30">
				<h3 style="text-align: center;">Phù hợp</h3>
				<h5 style="text-align: center;">Gia đinh</h5>


			</div>











			<h1 class="w-bot-border">
				Giới <span> thiệu</span>
			</h1>
			<p style="text-align: justify">Quis sed mid elit, risus aliquet
				placerat. Pid et, vel phasellus augue a ultrices, natoque sociis
				porta proin nec? Dictumst magna rhoncus quis diam! Nascetur non
				risus elit pellentesque mauris pulvinar purus tincidunt, ac urna,
				placerat mus porta, egestas ultrices turpis. Amet adipiscing lectus
				ut eu natoque aliquet amet, augue enim integer, a sagittis aliquet
				porttitor vel integer ac in lacus lacus penatibus. Pid pellentesque
				eu? Vel! Sed mattis augue purus odio dictumst velit duis, ut rhoncus
				ac integer.</p>
			<h1 class="w-bot-border">
				Cách <span>làm </span>
			</h1>
			<p style="text-align: justify">Habitasse egestas pulvinar? Ac? A
				egestas est sit augue! Ac est nunc mauris turpis, augue eu nisi vut
				et! Pulvinar! Scelerisque est parturient in, cras, et. Platea in et
				sit, et pulvinar tortor pid, sagittis. Risus porta. Rhoncus ut!
				Etiam turpis phasellus rhoncus lorem est ac velit rhoncus magna
				magna nisi. Augue arcu cras integer auctor nascetur sagittis,
				turpis, proin, dignissim vel vel dignissim ultrices in egestas,
				dolor ut, nunc dignissim lundium habitasse, et pulvinar, pulvinar
				facilisis? Eu nisi.</p>
			<h1 class="w-bot-border">
				Nguyên <span>liệu </span>
			</h1>
			<p style="text-align: justify">Phasellus porta rhoncus dis mauris
				magna risus proin nec scelerisque ultrices parturient porta nisi
				lectus. Integer tincidunt, dolor amet? Risus hac turpis ac sagittis
				mattis, aliquam tristique! Velit augue! Vel nec, proin turpis
				lectus, enim ultricies sociis aenean ac. Dis vut urna et et, augue!
				Hac dictumst, sed, aliquet? Massa, nunc augue, et, dictumst lacus
				sed! Quis placerat habitasse eros pellentesque in quis, et velit
				nisi sit, placerat mid ac placerat! Sagittis a pulvinar, sociis et
				nisi tincidunt diam.</p>
			<p>Lectus odio non habitasse. Facilisis sed mauris, integer urna
				mauris elit porttitor, habitasse, augue penatibus sit dictumst, arcu
				in? Eros, in penatibus, cursus, mus etiam porta ut amet nisi pid,
				sociis, dignissim sociis, a, nisi penatibus? Pellentesque cursus
				lectus scelerisque est, integer porttitor ac habitasse! Nisi, arcu,
				urna odio velit etiam, lundium odio. Lorem turpis. Habitasse vut
				odio pulvinar, dignissim magna, ridiculus a a, et integer, enim a
				mauris! Augue velit! Cum porttitor aliquet dignissim sagittis
				placerat parturient natoque.</p>
			<p>Tortor! Amet phasellus tincidunt ultrices nec, tempor magna
				duis velit massa cras, rhoncus hac a eros dictumst! Pulvinar
				adipiscing, sed dapibus diam tincidunt risus phasellus arcu dis
				adipiscing? A enim porta, turpis sagittis vut, integer augue amet
				cursus odio. Velit placerat a montes! Pulvinar rhoncus est odio, ac
				et, dictumst tincidunt proin a lundium tortor. Mid magnis nisi.
				Aenean enim ultricies rhoncus ultrices elementum, diam mauris ac?
				Magna ac ultrices ut? A in! Eu in dictumst sagittis? Purus
				penatibus.</p>

		</div>
		<!-- end of post div -->


		<div class="comments">
		<div id="whats-hot">
		<h2 style="margin: 20px 0px">
			Nguyên liệu <span> </span>
		</h2>
		<span class="w-pet-border"></span>

		<ul class="cat-list">
			<li style="margin-left:96px">
				<h3>
					<a href="recipe-single-1.html">Chocolate</a>
				</h3> <a href="recipe-single-1.html" class="img-box"> <img
					src="${pageContext.request.contextPath}/resources/static/images/demo/7a0a46455c4ec56a5a02c097374fc513-222x144.jpg"
					class="attachment-recipe-4column-thumb wp-post-image"
					alt="7a0a46455c4ec56a5a02c097374fc513" />
			</a>
				<h4>
					<a href="recipe-single-1.html"> Chocolate Earl Grey Pots... </a>
				</h4>
				<p>
					2 cups cream 120 grams dark chocolate, chopped 2 bags of earl grey
					tea 6 <a href="recipe-single-1.html">...more </a>
				</p>
			</li>
			
			<li>
				<h3>
					<a href="recipe-single-1.html">Rolls</a>
				</h3> <a href="recipe-single-1.html" class="img-box"> <img
					src="${pageContext.request.contextPath}/resources/static/images/demo/Goat-Cheese-Chorizo-Rolls2-222x144.jpg"
					class="attachment-recipe-4column-thumb wp-post-image"
					alt="Goat-Cheese-Chorizo-Rolls2" />
			</a>
				<h4>
					<a href="recipe-single-1.html"> Goat Cheese and Chorizo... </a>
				</h4>
				<p>
					You know how bacon is all the rage these days? Bacon dipped in
					chocolate, bacon <a href="recipe-single-1.html">...more </a>
				</p>
			</li>
			<li>
				<h3>
					<a href="recipe-single-1.html"> Economical </a>
				</h3> <a href="recipe-single-1.html" class="img-box"> <img
					src="${pageContext.request.contextPath}/resources/static/images/demo/accor_2-222x144.jpg"
					class="attachment-recipe-4column-thumb wp-post-image" alt="accor_2" />
			</a>
				<h4>
					<a href="recipe-single-1.html"> baking question </a>
				</h4>
				<p>
					Quis sed mid elit, risus aliquet placerat. Pid et, vel phasellus
					augue a ultrices, natoque <a href="recipe-single-1.html">...more
					</a>
				</p>
			</li>
		</ul>
	</div>

			<!-- You can start editing here. -->

			<h1 id="comments">
				Comment <span>(1)</span>
			</h1>
			<span class="w-pet-border"></span>

			<div class="container pb-cmnt-container">
				<div class="row">
					<div class="col-md-9">
						<div class="panel panel-info">
							<div class="panel-body">
								<textarea cols=111 placeholder="Write your comment here!"
									class="pb-cmnt-textarea"></textarea>
								<form class="form-inline">
									<div class="btn-group">
										<button class="btn" type="button">
											<span class="fa fa-picture-o fa-lg"></span>
										</button>
										<button class="btn" type="button">
											<span class="fa fa-video-camera fa-lg"></span>
										</button>
										<button class="btn" type="button">
											<span class="fa fa-microphone fa-lg"></span>
										</button>
										<button class="btn" type="button">
											<span class="fa fa-music fa-lg"></span>
										</button>
									</div>
									<button class="btn btn-primary pull-right" type="button">Share</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<ol class="comment-list">
				<li
					class="comment byuser comment-author-admin bypostauthor even thread-even depth-1"
					id="comment-7">

					<div class="img-box">
						<a href="#"> <img
							src="http://0.gravatar.com/avatar/66a10cd6fc9b85636291aa4fe7c32c7f?s=67&amp;d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D67&amp;r=G"
							class="avatar avatar-67 photo" alt="">
						</a>
					</div>
					<div class="comment-body" style="width: 91%;">
						<a style="font-size: 15px; font-weight: 600;" class="meta">posted
							by admin on <span> March 24, 2015 </span>
						</a>

						<div class="comment">
							<p>Habitasse egestas pulvinar? Ac? A egestas est sit augue!
								Ac est nunc mauris turpis, augue eu nisi vut et! Pulvinar!
								Scelerisque est parturient in,</p>
						</div>

						<div class="reply">
							<button class="btn btn btn-danger comment-reply-link" href="#"
								aria-label="Reply to admin">Báo cáo </button>
						</div>

					</div>

				</li>
				<!-- #comment-## -->
			</ol>



		</div>
		<!-- end of comments div -->


	</div>


</div>



<jsp:include page="layout/_footer.jsp"></jsp:include>